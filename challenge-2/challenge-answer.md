```javascript
export function solution(A, B) {
  let aArr = Array(A).fill('a');
  let bArr = Array(B).fill('b');
  let final = [];
  let lastChar = '';

  while( (aArr.length + bArr.length) > 0) {         

    if(final.length > 0 && final[final.length - 1] == final[final.length-2]) {
      final.push(
        lastChar == aArr[0] ? bArr.pop() : aArr.pop()
      )
    } else {
      final.push((aArr.length > bArr.length ? aArr : bArr).pop());
    }

    lastChar = final[final.length-1];

  }

  return final.join('');
}

window.addEventListener('DOMContentLoaded', function() {
  console.log(solution(3,5));
  console.log(solution(1,3));
  console.log(solution(5,3));
  console.log(solution(3,1));
  console.log(solution(3,3));
  console.log(solution(5,5));
  console.log(solution(9,5));
});
```
