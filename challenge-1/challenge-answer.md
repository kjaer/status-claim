```javascript

function getComments(count) {
  return $.getJSON(`https://www.example.com/comments?count=${count}`);
}

function createCommentItem(comment) {
  if (!comment){
    return "";
  }

  // comment Item
  const commentItem = document.createElement('div');
  commentItem.setAttribute('class', 'comment-item');
  commentItem.setAttribute('id', comment.id);

  const userName = document.createElement('div');
  userName.setAttribute('class', 'comment-item__username');
  userName.innerHTML =  comment.username;

  const message = document.createElement('div');
  message.setAttribute('class', 'comment-item__message');
  message.innerHTML = comment.message;

  commentItem.appendChild(userName);
  commentItem.appendChild(message);

  return commentItem;
}

function loading(commentListNode) {
  commentListNode.innerHTML = 'Loading...';
}

function createCommentList(promiseList) {
  return Promise.all(promiseList.map(promise => promise.fail(error => [])));
}

function getAllCommentListNodes() {
  return document.querySelectorAll('.comment-list');
}

function commentFill(commentListNode) {
  commentListNode.innerHTML = "";

  return function(comments){
    comments.forEach(comment => {
      commentListNode.appendChild(comment);
    })
  }
}

function solution() {
  const commentLists = Array.from(getAllCommentListNodes());
  const promiseList = commentLists.map(commentNode => getComments(parseInt(commentNode.dataset.count)));

  commentLists.forEach(commentListNode => { loading(commentListNode) });

  createCommentList(promiseList)
  .then(commentResponse => {
    return commentResponse.map((comments) => comments.map(createCommentItem))
  })
  .then(commentItemList => {
    const lists = commentLists.map(commentListNode => commentFill(commentListNode));
    lists.forEach((list, index) => list(commentItemList[index]));
  })
}

window.addEventListener('DOMContentLoaded', (event) => {
  solution();
});

```