You would like to add the ability to render comments on a previously static website. The comments are submitted only via a mobile app, so the website itself just needs to display the latest comments. Comments should be fetched on the client (browser) side and then displayed inside all tags that have the class comment-list (there can be more than one such tag). All of these elements will have a data-count attribute determining the number of comments to fetch.

Here's an example of such a tag:

```html
<div class="comment-list" data-count=10></div>
```
The comment data for this tag can be obtained by making an AJAX call to the following endpoint:

https://www.example.com/comments?count=10

In the example above we are asking for ten comments. The response format is as follows:
```json
[{
    "id": 123,
    "username": "User 1",
    "message": "Cool product!"
}, {
    "id": 456,
    "username": "User 2",
    "message": "Didn't like it that much"
},
   ...
]
```
While the comments are being loaded, the comment-list elements should be filled with the text Loading....

The DOM structure of each comment is expected to be:
```html
<div class="comment-item">
  <div class="comment-item__username">{username}</div>
  <div class="comment-item__message">{message}</div>
</div>
```
The comments should appear one after the other, with the order preserved.

If any error occurs during the loading, the tag should be left empty.

Write a function:
```javascript
function solution();
```
that, given a DOM tree representing an HTML document, renders comments as described above.

You can use either the XMLHttpRequest or the fetch API. Please remember that using xhr.async = false; will not work. You can also use jQuery.ajax and similar methods to access the API.

For example, given a document containing
```html
<div class="comment-list" data-count=2></div>
```
and the response:
```json
[{
    "id": 123,
    "username": "User 1",
    "message": "Cool product!"
}, {
    "id": 456,
    "username": "User 2",
    "message": "Didn't like it that much"
}]
```
then afterwards the document should contain:
```html
<div class="comment-list" data-count=2>
  <div class="comment-item">
    <div class="comment-item__username">User 1</div>
    <div class="comment-item__message">Cool product!</div>
  </div>
  <div class="comment-item">
    <div class="comment-item__username">User 2</div>
    <div class="comment-item__message">Didn't like it that much</div>
  </div>
</div>
```
In another example, given a document containing
```html
<div class="comment-list" data-count=2></div>
<div class="comment-list" data-count=2></div>
```
and the same response as in the previous example, then afterwards the document should contain:
```html
<div class="comment-list" data-count=2>
  <div class="comment-item">
    <div class="comment-item__username">User 1</div>
    <div class="comment-item__message">Cool product!</div>
  </div>
  <div class="comment-item">
    <div class="comment-item__username">User 2</div>
    <div class="comment-item__message">Didn't like it that much</div>
  </div>
</div>
<div class="comment-list" data-count=2>
  <div class="comment-item">
    <div class="comment-item__username">User 1</div>
    <div class="comment-item__message">Cool product!</div>
  </div>
  <div class="comment-item">
    <div class="comment-item__username">User 2</div>
    <div class="comment-item__message">Didn't like it that much</div>
  </div>
</div>
```
In yet another example, you are given a document containing
```html
<div class="comment-list" data-count=1></div>
```
but an error occurs while loading comments. In this case, the document should remain the same.

Assumptions:

the DOM tree represents a valid HTML5 document;
jQuery 2.1.4 is supported;
the data-count parameter will be in the range 1 to 100;
the AJAX call response text will contain valid JSON with no more than the data-count number of comments;
the response data may or may not be sanitised;
the elements with the comment-list class cannot be inside other such elements.*/