# Challenge 1
This is a time-boxed coding challenge on platform called codility,
I spent 50-70 minutes to accomplish this.

* [Description](challenge-description.md)
* [Answer](challenge-answer.md)
