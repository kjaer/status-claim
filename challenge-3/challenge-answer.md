```javascript
function solution3(S, K) {
  const text = S.trim().split(" ");
  
  const abort = text.some(word => word.length > K);

  if(abort) {
    return -1;
  }
  
  let message = text.reduce((sms, word) => {
    const wordLength = word.length;
    const lastWordLength = sms[sms.length-1].length;
    
    // +1 for space between words.
    if((wordLength + lastWordLength +1) <= K) {
      sms[sms.length-1] = (`${sms[sms.length-1]} ${word}`).trim();
    } else {
      sms.push(word);
    }

    return sms;
  }, [""])

  return message;
 }

 window.addEventListener('DOMContentLoaded', function() {
  console.log(solution3("SMS messages are really short", 12));
  console.log(solution3("Limittendahauzunolankelimesi olan mesaj arrayi", 12));
  console.log(solution3(" olan mesaj Limitten daha uzunolan kelimesi arrayi", 12));
  console.log(solution3("Lorem Ipsum dolor sit amet", 12))
 });
```
