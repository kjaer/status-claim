You are given an HTML document with a form and your task is to validate it in JavaScript.

The form is used to enter contact data, either of a person or a company. The radio buttons named "type" control this choice. Exactly one such button (of value "person" or "company") will be checked.

If the "person" option is checked, you should validate the following fields (and only these):

- first_name and last_name should be non-empty. For example, "John" and " " (space) are valid, but "" is not.
- email should be non-empty and validated according to a simple scheme: 
    - It should contain exactly one @ character. 
    - Moreover, both parts (before and after the @) should consist of at least one and at most 64 characters comprising only letters, digits and/or dots (a−z, A−Z, 0−9, .) (For example, "john@example.com" and ".@." are valid, but "@example.com" is not).

If the "company" option is checked, you should validate the following fields (and only these):

- company_name should be non-empty;

- phone should be non-empty 
    - and consist of digits, dashes (-) and spaces only. 
    - It should have at least six digits. For example, "234-567-890" is valid, but "12-3" is not.

Write a function
 function solution();

that, given a DOM tree representing an HTML document, returns a Boolean value indicating whether the form in this document is valid.

For example, given an HTML document with the following content within the <body> tag:
```html
<form>
  <input type="radio" id="type_person" name="type" value="person" checked/>
  <input type="radio" id="type_company" name="type" value="company"/>
  <input type="text" id="first_name" name="first_name" value="John"/>
  <input type="text" id="last_name" name="last_name" value="Doe"/>
  <input type="text" id="email" name="email" value="john@example.com"/>
  <input type="text" id="company_name" name="company_name" value=""/>
  <input type="text" id="phone" name="phone" value="234-567-890"/>
</form>
```

your function should return true, while given the following content:
```html
<form>
  <input type="radio" id="type_person" name="type" value="person"/>
  <input type="radio" id="type_company" name="type" value="company" checked/>
  <input type="text" id="first_name" name="first_name" value="John"/>
  <input type="text" id="last_name" name="last_name" value="Doe"/>
  <input type="text" id="email" name="email" value="john@example.com"/>
  <input type="text" id="company_name" name="company_name" value="ACME"/>
  <input type="text" id="phone" name="phone" value="12-3"/>
</form>
```

your function should return false (the phone number is not correct).

Assume that:

- the DOM tree represents a valid HTML5 document;
- there is exactly one form in the document and it contains all necessary fields;
- the length of the HTML document does not exceed 4KB;
- jQuery 2.1 is supported.

