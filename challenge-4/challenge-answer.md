 ```javascript
    const PERSON = 'person';
    const COMPANY = 'company';
    const MINPHONENUMBERLENGTH = 6;
    const MINEMAILLENGTH = 1;
    const MAXEMAILLENGTH = 64;

    const validations = {
      email: (fieldValue) => {

        if (!fieldValue.includes("@")) {
          return false;
        }

        // if email address includes more than one @ character.
        const countOfAt = fieldValue.match(/@+/g).length;
        if (countOfAt >= 2) {
          return false;
        }

        const emailRegex = /^[a-zA-Z0-9.]+$/;
        const emailFields = fieldValue.split("@");
        const properEmail = emailFields.every(field => emailRegex.test(field));
        const lengthFields = emailFields.every(field => field.length >= MINEMAILLENGTH && field.length <= MAXEMAILLENGTH);

        return properEmail && lengthFields;

      },
      phone: (fieldValue) => {
        const phoneRegex = /^[0-9\s\-]+$/;

        if (!phoneRegex.test(fieldValue)) {
          return false;
        }
        // https://codereview.stackexchange.com/questions/115885/extract-numbers-from-a-string-javascript
        const numbers = fieldValue.match(/\d+/g).join("");

        return numbers.length >= MINPHONENUMBERLENGTH;
      },
      nonEmpty: (fieldValue) => fieldValue !== ""
    }

    const validateFormRules = {
      [PERSON]: [
        { selector: "#first_name", validation: validations.nonEmpty },
        { selector: "#last_name", validation: validations.nonEmpty },
        { selector: "#email", validation: [validations.nonEmpty, validations.email] }
      ],
      [COMPANY]: [
        { selector: "#company_name", validation: validations.nonEmpty },
        { selector: "#phone", validation: [validations.nonEmpty, validations.phone] }
      ]
    };

    function getFormType(form) {
      const formType = form.querySelector("[name='type']:checked").value;
      return formType == PERSON ? PERSON : COMPANY;
    }

    function executeValidations(fieldValue, validation) {
      if (Array.isArray(validation)) {
        return validation.every(validation => validation(fieldValue))
      }

      return validation(fieldValue)
    }

    function solution() {
      const form = document.forms[0];
      const formType = getFormType(form);

      return validateFormRules[formType]
        .map(rule => executeValidations(form.querySelector(rule.selector).value, rule.validation))
        .every(Boolean);
    }

    window.addEventListener('DOMContentLoaded', function () {
      const result = solution();
      console.log(result);
    })
```
